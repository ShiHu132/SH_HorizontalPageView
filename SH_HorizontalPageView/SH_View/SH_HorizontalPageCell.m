//
//  SH_HorizontalPageCell.m
//  SH_HorizontalPageView
//
//  QQ:1224614774  昵称: 嗡嘛呢叭咪哄
//  QQ群:807236138  群称: iOS 技术交流学习群

//  Created by 石虎 on 2019/12/12.
//  Copyright © 2019 石虎. All rights reserved.
//

#import "SH_HorizontalPageCell.h"

@implementation SH_HorizontalPageCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
        [self setupLayouts];
    }
    return self;
}

- (void)setupViews {
    
    [self.contentView addSubview:self.titleLbl];
}

- (void)setupLayouts {
    self.titleLbl.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (UILabel *)titleLbl {
    
    if (!_titleLbl) {
        _titleLbl               = [[UILabel alloc] init];
        _titleLbl.font          = [UIFont systemFontOfSize:20];
        _titleLbl.textColor     = [UIColor blackColor];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLbl;
}

@end
