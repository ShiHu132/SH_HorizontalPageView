//
//  SH_HorizontalPageCell.h
//  SH_HorizontalPageView
//
//  QQ:1224614774  昵称: 嗡嘛呢叭咪哄
//  QQ群:807236138  群称: iOS 技术交流学习群

//  Created by 石虎 on 2019/12/12.
//  Copyright © 2019 石虎. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SH_HorizontalPageCell : UICollectionViewCell

/** 标题  */
@property (nonatomic, strong) UILabel * titleLbl;

@end

NS_ASSUME_NONNULL_END
