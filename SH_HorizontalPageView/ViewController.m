//
//  ViewController.m
//  SH_HorizontalPageView

//  QQ:1224614774  昵称: 嗡嘛呢叭咪哄
//  QQ群:807236138  群称: iOS 技术交流学习群

//  Created by 石虎 on 2019/12/12.
//  Copyright © 2019 石虎. All rights reserved.
//

#import "ViewController.h"
#import "SH_HorizontalPageCell.h"
#import "SH_HorizontalPageFlowlayout.h"

#define KScreenWidth  [UIScreen mainScreen].bounds.size.width
#define KScreenHeight [UIScreen mainScreen].bounds.size.height
#define KRandom(X)    arc4random_uniform(X)/255.0

@interface ViewController ()
<
UICollectionViewDataSource,
UICollectionViewDelegate
>

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.collectionView];
}

#pragma mark - Lazy

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        
        // 自定义SH_HorizontalPageFlowlayout布局
        SH_HorizontalPageFlowlayout *layout = [[SH_HorizontalPageFlowlayout alloc] initWithRowCount:2 itemCountPerRow:4];
        // 设置行列间距及collectionView的内边距
        [layout sh_ColumnSpacing:10 rowSpacing:15 edgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
        // 同一个section 内部item的竖直方向间隙
        layout.minimumInteritemSpacing = 0;
        // 同一个section 内部 item的水平方向间隙
        layout.minimumLineSpacing = 0;
        // 水平滑动
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        // 初始化
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 200, KScreenWidth, 300) collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor grayColor];
        _collectionView.bounces = YES;
        // 翻过整页
        _collectionView.pagingEnabled = YES;
        // 遵守代理
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        // 纯代码注册
         [_collectionView registerClass:[SH_HorizontalPageCell class] forCellWithReuseIdentifier:NSStringFromClass([SH_HorizontalPageCell class])];
    }
    return _collectionView;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 1000;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SH_HorizontalPageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SH_HorizontalPageCell class]) forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:KRandom(255) green:KRandom(255) blue:KRandom(255) alpha:1.0];
    NSInteger row = indexPath.row;
    cell.titleLbl.text = [NSString stringWithFormat:@"第%ld个", (long)row];
    
    return cell;
}

@end
