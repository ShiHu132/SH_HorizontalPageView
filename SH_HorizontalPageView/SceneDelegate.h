//
//  SceneDelegate.h
//  SH_HorizontalPageView
//
//  QQ:1224614774  昵称: 嗡嘛呢叭咪哄
//  QQ群:807236138  群称: iOS 技术交流学习群

//  Created by 石虎 on 2019/12/12.
//  Copyright © 2019 石虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

